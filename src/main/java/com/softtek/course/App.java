package com.softtek.course;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        System.out.println("La suma de 2 + 2 es: " + suma(2, 2));
        System.out.println("La resta de 4 - 2 es: " + resta(4, 2));
    }


    public static int suma(int a, int b){
        return a+b;
    }

    public static int resta(int a, int b){
        return a-b;
    }
}
